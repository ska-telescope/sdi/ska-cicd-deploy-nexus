# Deploy Nexus for the Central Artefact Repository

Deploy Nexus OSS edition for the Central Artefact Repository on AWS.

## Summary

This repo deploys Nexus OSS edition using an adpated version of https://github.com/ansible-ThoTeam/nexus3-oss. The changes are found in the `./resources` directory, and are used to overwrite the freshly checked out roles held in the `./collections` directory.

To use this repository:
```
$ git clone git@gitlab.com:ska-telescope/sdi/deploy-nexus.git
$ cd deploy-nexus
$ make reinstall # installs the necessary roles and collections in `./collections`
```

Edit the `ssh.config` and `./inventory_nexus` files appropriate to your target host. and then create a `./PrivateRules.mak` file containing the passwords for the Nexus user accounts `admin`,`gitlab`, `publisher` and `quarantiner`, as well as the webhook url and secret key as follows:
```
EXTRA_VARS = vault_nexus_admin_password='...' vault_nexus_user_password_gitlab='...' vault_nexus_user_password_publisher='...' vault_nexus_ldap_conn_passwd='...' vault_nexus_email_server_password='...' vault_nexus_user_password_quarantiner='...' nexus_webhook_url='...' nexus_webhook_secret_key='...'
```

Then build using:
```
$ make nexus
```

## Deployment

The basic deployment consists of:
* a role (common) that sets basic host configuration
* the system-common-roles docker role that installs a standard Docker daemon footprint
* The modified Nexus role from https://github.com/ansible-ThoTeam/nexus3-oss#example-playbook

This sets up and deploys Nexus, which is then frontended with HAProxy runnning in a docker container.  HAProxy does the URI and port mapping necessary to expose Nexus and the Docker repository.

The nexus3-oss role is patched  by the files found in the `./resources` directory.


## Production site

The production Nexus instance for the Central Artefact Repository is hosted at https://artefact.skatelescope.org/.  This is integrated with SKAO LDAP based authentication for administration access - all other service accounts are maintained as local users.

## Configuration

The complete configuration for the deployment is contained in [nexus.yml](./playbooks/nexus.yml).


## Development

As this repo relies on a modified upstream version of https://github.com/ansible-ThoTeam/nexus3-oss, the differences are maintained as a patch file in the `./resources` directory.
The patch can be maintained by checking out upstream with:
```
$ make update-patch
```
This creates the checked out code in `./nexus3-oss`, and applies the (uncommited) current version of the patch.  Edit the repository as required (without commiting the changes), and regenerate the patch file with:
```
$ make make-patch
```
This will update the path in `./resources`.  To activate these new changes, the `./collections` directory needs to be regenerated with:
```
$ make reinstall
$ make patch
```

## make help

Run `make` to get the help:
```
$ make
make targets:
Makefile:build_nexus           Install nexus
Makefile:help                  show this help.
Makefile:install               Install dependent ansible collections
Makefile:lint                  Lint check playbook
Makefile:reinstall             reinstall collections
Makefile:vars                  List Variables

make vars (+defaults):
Makefile:COLLECTIONS_PATHS     ./collections
Makefile:COLLECTIONS_VERSION   v2.4.14
Makefile:EXTRA_VARS            vault_nexus_admin_password='whatwhat' vault_nexus_user_password_gitlab='whatwhat' vault_nexus_user_password_publisher='whatwhat'
Makefile:INVENTORY_FILE        ./inventory_nexus
Makefile:NODES                 localhost
Makefile:PRIVATE_VARS          ./nexus_vars.yml
```
