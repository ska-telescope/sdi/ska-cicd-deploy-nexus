#!/bin/sh

echo "Installing dependencies using shell"
echo "Current path is: ${PWD}"

echo "Installing collections:"
ansible-galaxy collection install -r ${PWD}/molecule/default/requirements.yml

cp -f ${PWD}/../resources/nexus_install.yml \
    ${PWD}/../collections/ansible-thoteam.nexus3-oss/tasks/nexus_install.yml
cp -f ${PWD}/../resources/process_repos_list.yml \
    ${PWD}/../collections/ansible-thoteam.nexus3-oss/tasks/process_repos_list.yml
cp -f ${PWD}/../resources/setup_realms.groovy \
    ${PWD}/../collections/ansible-thoteam.nexus3-oss/files/groovy/setup_realms.groovy


echo "Installing roles:"
mkdir -p ${HOME}/.ansible/roles
cp -r ${PWD}/../playbooks/roles/* ${HOME}/.ansible/roles/
cp -r ${PWD}/../collections/ansible-thoteam.nexus3-oss ${HOME}/.ansible/roles/
cp -r ${PWD}/../collections/ansible_collections/* ${HOME}/.ansible/roles/
ls -latr ${HOME}/.ansible/roles/
